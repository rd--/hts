all:
	echo "hts"

mk-cmd:
	echo "hts - NIL"

clean:
	rm -fR dist dist-newstyle *~

push-all:
	r.gitlab-push.sh hts

push-tags:
	r.gitlab-push.sh hts --tags

indent:
	fourmolu -i Music

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Music
