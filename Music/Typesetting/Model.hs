module Music.Typesetting.Model where

import qualified Music.Theory.Clef as T {- hmt -}
import qualified Music.Theory.Duration as T
import qualified Music.Theory.Dynamic_Mark as T {- hmt -}
import qualified Music.Theory.Key as T {- hmt -}
import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Pitch.Note as T {- hmt -}
import qualified Music.Theory.Tempo_Marking as T {- hmt -}
import qualified Music.Theory.Time_Signature as T {- hmt -}

type Font_Family_Type = String

data Font_Style_Type
  = Font_Style_Normal
  | Font_Style_Italic
  deriving (Eq, Ord, Enum, Show)

type Font_Size_Type = Int

data Font_Weight_Type
  = Font_Weight_Normal
  | Font_Weight_Bold
  deriving (Eq, Ord, Enum, Show)

data Font_Type = Font Font_Family_Type Font_Style_Type Font_Size_Type Font_Weight_Type
  deriving (Eq, Ord, Show)

data Articulation_Type
  = Accent
  | Staccato
  | Strong_Accent
  | Tenuto
  deriving (Eq, Ord, Enum, Show)

data Ornament_Type = Trill_Mark
  deriving (Eq, Ord, Enum, Show)

data Harmonic_Type
  = Natural_Harmonic
  | Artifical_Harmonic
  deriving (Eq, Ord, Enum, Show)

data Technical_Type
  = Up_Bow
  | Down_Bow
  | Harmonic Harmonic_Type
  | Open_String
  | Stopped
  | Snap_Pizzicato
  | Other_Technical (Maybe Font_Type) String
  deriving (Eq, Ord, Show)

data Placement_Type = Above | Below
  deriving (Eq, Ord, Enum, Show)

type Tuplet_Type = (Integer, T.Duration, Integer, T.Duration)

data Pedal_Type = Pedal_Start | Pedal_Stop | Pedal_Continue | Pedal_Change
  deriving (Eq, Ord, Enum, Show)

data Sound_Type
  = Sound_Tempo Double -- q = mm
  | Sound_Dynamics Double -- % of F (where F=90)
  deriving (Eq, Ord, Show)

data Direction_Type
  = D_Rehearsal String
  | D_Words Placement_Type String
  | D_Hairpin T.Hairpin
  | D_Dynamic_Mark T.Dynamic_Mark
  | D_Pedal
      { pedal_type :: Pedal_Type
      , pedal_line :: Bool
      , pedal_sign :: Bool
      }
  | D_Tempo_Marking T.Tempo_Marking {- metronome -}
  deriving (Eq, Ord, Show)

data Beam_Type = Beam_Begin | Beam_Continue | Beam_End
  deriving (Eq, Ord, Enum, Show)

data Notehead_Type
  = Notehead_Triangle
  | Notehead_Diamond
  | Notehead_Square
  | Notehead_Cross
  deriving (Eq, Ord, Enum, Show)

-- | Ordered to meet musicxml requirements.
data N_Annotation
  = N_Grace
  | N_Chord
  | N_Pitch T.Pitch
  | N_Unpitched -- N_Pitch should allow for NO accidental?
  | N_Rest
  | N_Notehead Notehead_Type
  | N_Staff Integer
  | N_Beam Integer Beam_Type
  | -- BEGIN NOTATIONS
    N_Begin_Tied
  | N_End_Tied
  | N_Begin_Slur
  | N_End_Slur
  | N_Begin_Tuplet (Maybe Tuplet_Type)
  | N_End_Tuplet
  | N_Begin_Glissando
  | N_End_Glissando
  | N_Begin_Slide
  | N_End_Slide
  | N_Stem_Tremolo Integer
  | N_Ornament Ornament_Type
  | N_Technical Technical_Type
  | N_Articulation Articulation_Type
  | N_Fermata
  | N_Arpeggiate
  | -- END NOTATIONS
    N_Direction Direction_Type
  | N_Voice Integer
  | N_Backup [T.Duration]
  deriving (Eq, Ord, Show)

data Note = Note
  { n_duration :: T.Duration
  , n_annotations :: [N_Annotation]
  }
  deriving (Eq, Show)

data M_Annotation
  = M_Division Integer
  | M_Key T.Note (Maybe T.Alteration) T.Mode
  | M_Time_Signature T.Time_Signature
  | M_Staves Integer
  | M_Clef (T.Clef Integer) Integer
  | M_Direction Direction_Type
  deriving (Eq, Ord, Show)

data Measure = Measure
  { m_annotations :: [M_Annotation]
  , m_notes :: [Note]
  }
  deriving (Eq, Show)

type Name = (String, String)

data Group_Symbol_Type
  = None
  | Brace
  | Line
  | Bracket
  deriving (Eq, Show)

newtype P_Annotation = P_Name Name deriving (Eq, Show)

data G_Annotation
  = G_Name Name
  | G_Symbol Group_Symbol_Type
  deriving (Eq, Show)

type ID = Integer

{- | The ID fields must be set when writing XML, see 'part_set_id'.
  The P_Name P_Annotation is required.
-}
data Part
  = Part (Maybe ID) [P_Annotation] [Measure]
  | Group (Maybe ID) [G_Annotation] [Part]
  deriving (Eq, Show)

newtype Score = Score [Part] deriving (Eq, Show)
