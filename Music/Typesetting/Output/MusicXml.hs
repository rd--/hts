module Music.Typesetting.Output.MusicXml where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Data.Ratio {- base -}

import qualified Text.XML.Light as X {- xml -}

import qualified Music.Theory.Clef as T {- hmt -}
import qualified Music.Theory.Duration as T {- hmt -}
import qualified Music.Theory.Duration.Rq as T {- hmt -}
import qualified Music.Theory.Dynamic_Mark as T {- hmt -}
import qualified Music.Theory.Key as T {- hmt -}
import qualified Music.Theory.Pitch as T {- hmt -}
import qualified Music.Theory.Pitch.Note as T {- hmt -}

import qualified Music.Theory.Tempo_Marking as T {- hmt -}
import qualified Music.Theory.Time_Signature as T {- hmt -}

import Music.Typesetting.Model {- hts -}
import qualified Music.Typesetting.Output.MusicXml.Binding as M {- hts -}
import qualified Music.Typesetting.Process as P {- hts -}

x_clef_t :: T.Clef_Type -> (String, Integer)
x_clef_t c =
  case c of
    T.Treble -> ("G", 2)
    T.Alto -> ("C", 3)
    T.Tenor -> ("C", 4)
    T.Bass -> ("F", 4)
    T.Percussion -> ("percussion", 3)

x_clef :: T.Clef Integer -> Integer -> X.Content
x_clef (T.Clef c i) n =
  let (s, l) = x_clef_t c
      a = [M.number (show n)]
  in M.clef
      a
      [ M.sign [] [M.cdata s]
      , M.line [] [M.cdata (show l)]
      , M.clef_octave_change [] [M.cdata (show i)]
      ]

x_key_mode_t :: T.Mode -> String
x_key_mode_t md =
  case md of
    T.Major_Mode -> "major"
    T.Minor_Mode -> "minor"

x_key :: (T.Note, Maybe T.Alteration, T.Mode) -> X.Content
x_key (n, a, m) =
  let a' = fromMaybe T.Natural a
      k = fromMaybe (error "x_key?") (T.key_fifths (n, a', m))
  in M.key
      []
      [ M.fifths [] [M.cdata (show k)]
      , M.mode [] [M.cdata (x_key_mode_t m)]
      ]

x_time :: T.Time_Signature -> X.Content
x_time (n, d) =
  M.time
    []
    [ M.beats [] [M.cdata (show n)]
    , M.beat_type [] [M.cdata (show d)]
    ]

x_pitch :: T.Pitch -> X.Content
x_pitch (T.Pitch n a o) =
  M.pitch
    []
    [ M.step [] [M.cdata (show n)]
    , M.alter [] [M.cdata (show (T.alteration_to_fdiff a :: Double))]
    , M.octave [] [M.cdata (show o)]
    ]

x_alteration_t :: T.Alteration -> String
x_alteration_t x =
  case x of
    T.DoubleFlat -> "double-flat"
    T.ThreeQuarterToneFlat -> "three-quarters-flat"
    T.Flat -> "flat"
    T.QuarterToneFlat -> "quarter-flat"
    T.Natural -> "natural"
    T.QuarterToneSharp -> "quarter-sharp"
    T.Sharp -> "sharp"
    T.ThreeQuarterToneSharp -> "three-quarters-sharp"
    T.DoubleSharp -> "double-sharp"

x_pitch_accidental :: T.Pitch -> X.Content
x_pitch_accidental (T.Pitch _ a _) =
  M.accidental [] [M.cdata (x_alteration_t a)]

x_multiplier :: Rational -> X.Content
x_multiplier x =
  let (n, d) = (numerator x, denominator x)
  in M.time_modification
      []
      [ M.actual_notes [] [M.cdata (show d)]
      , M.normal_notes [] [M.cdata (show n)]
      ]

-- due to absurd ordering requirements the duration information
-- is collected in three parts (duration,type+dots,multipler)
x_duration :: T.Duration -> (X.Content, [X.Content], Maybe X.Content)
x_duration d =
  let (T.Duration dv dt m) = d
      n = P.duration_rq_to_dv (T.duration_to_rq d)
      ty = T.whole_note_division_to_musicxml_type dv
      dt' = genericReplicate dt (M.dot [])
      m' = if m == 1 then Nothing else Just (x_multiplier m)
  in ( M.duration [] [M.cdata (show n)]
     , M.type_E [] [M.cdata ty] : dt'
     , m'
     )

x_tuplet_t_elem :: (Integer, T.Duration) -> [X.Content]
x_tuplet_t_elem (n, d) =
  let (T.Duration dv dt _) = d
      ty = T.whole_note_division_to_musicxml_type dv
      dt' = genericReplicate dt (M.tuplet_dot [])
  in [ M.tuplet_number [] [M.cdata (show n)]
     , M.tuplet_type [] [M.cdata ty]
     ]
      ++ dt'

x_tuplet_t :: Maybe Tuplet_Type -> [X.Content]
x_tuplet_t t =
  case t of
    Nothing -> []
    Just (an, ad, nn, nd) ->
      let a = x_tuplet_t_elem (an, ad)
          n = x_tuplet_t_elem (nn, nd)
      in [M.tuplet_actual [] a, M.tuplet_normal [] n]

x_ornament_t :: Ornament_Type -> X.Content
x_ornament_t = M.mk_empty_elem_no_attr . map c_hs_to_xml . show

x_ornament :: N_Annotation -> Maybe X.Content
x_ornament x =
  case x of
    N_Ornament d -> Just (x_ornament_t d)
    N_Stem_Tremolo i -> Just (M.tremolo [M.type_A "single"] [M.cdata (show i)])
    _ -> Nothing

x_ornaments :: [N_Annotation] -> Maybe X.Content
x_ornaments xs =
  case mapMaybe x_ornament xs of
    [] -> Nothing
    xs' -> Just (M.ornaments [] xs')

x_font_style :: Font_Style_Type -> String
x_font_style st =
  case st of
    Font_Style_Normal -> "normal"
    Font_Style_Italic -> "italic"

x_font_weight :: Font_Weight_Type -> String
x_font_weight wh =
  case wh of
    Font_Weight_Normal -> "normal"
    Font_Weight_Bold -> "bold"

x_font_attr :: Maybe Font_Type -> [X.Attr]
x_font_attr =
  let f (Font fm st sz wh) =
        [ M.font_family fm
        , M.font_style (x_font_style st)
        , M.font_size (show sz)
        , M.font_weight (x_font_weight wh)
        ]
  in maybe [] f

x_technical_el :: N_Annotation -> Maybe X.Content
x_technical_el x =
  case x of
    N_Technical t ->
      Just
        ( case t of
            Up_Bow -> M.up_bow []
            Down_Bow -> M.down_bow []
            Open_String -> M.open_string []
            Stopped -> M.stopped []
            Snap_Pizzicato -> M.snap_pizzicato []
            Harmonic Artifical_Harmonic -> M.harmonic [] [M.artificial []]
            Harmonic Natural_Harmonic -> M.harmonic [] [M.natural []]
            Other_Technical f s -> M.other_technical (x_font_attr f) s
        )
    _ -> Nothing

x_technical :: [N_Annotation] -> Maybe X.Content
x_technical xs =
  case mapMaybe x_technical_el xs of
    [] -> Nothing
    xs' -> Just (M.technical [] xs')

x_placement_t :: Placement_Type -> X.Attr
x_placement_t = M.placement . map toLower . show

c_underscore_to_hyphen :: Char -> Char
c_underscore_to_hyphen x = if x == '_' then '-' else x

c_hs_to_xml :: Char -> Char
c_hs_to_xml = toLower . c_underscore_to_hyphen

x_articulation_t :: Articulation_Type -> X.Content
x_articulation_t = M.mk_empty_elem_no_attr . map c_hs_to_xml . show

x_articulation :: N_Annotation -> Maybe X.Content
x_articulation x =
  case x of
    N_Articulation d -> Just (x_articulation_t d)
    _ -> Nothing

x_articulations :: [N_Annotation] -> Maybe X.Content
x_articulations xs =
  case mapMaybe x_articulation xs of
    [] -> Nothing
    xs' -> Just (M.articulations [] xs')

x_dynamic_mark_t :: T.Dynamic_Mark -> X.Content
x_dynamic_mark_t = M.mk_empty_elem_no_attr . map toLower . show

{-
x_dynamic_mark :: N_Annotation -> Maybe X.Content
x_dynamic_mark x =
    case x of
      N_Dynamic_Mark d -> Just (x_dynamic_mark_t d)
      _ -> Nothing

x_dynamics :: [N_Annotation] -> Maybe X.Content
x_dynamics xs =
    case mapMaybe x_dynamic_mark xs of
       [] -> Nothing
       xs' -> Just (dynamics [x_placement_t Below] xs')
-}

x_notation :: N_Annotation -> Maybe X.Content
x_notation x =
  case x of
    N_Begin_Tied -> Just (M.tied [M.type_A "start"])
    N_End_Tied -> Just (M.tied [M.type_A "stop"])
    N_Begin_Slur -> Just (M.slur [M.type_A "start"])
    N_End_Slur -> Just (M.slur [M.type_A "stop"])
    N_Begin_Tuplet t -> Just (M.tuplet [M.type_A "start"] (x_tuplet_t t))
    N_End_Tuplet -> Just (M.tuplet [M.type_A "stop"] [])
    N_Begin_Glissando -> Just (M.glissando [M.type_A "start"] [])
    N_End_Glissando -> Just (M.glissando [M.type_A "stop"] [])
    N_Begin_Slide -> Just (M.slide [M.type_A "start"] [])
    N_End_Slide -> Just (M.slide [M.type_A "stop"] [])
    N_Fermata -> Just (M.fermata [] [])
    N_Arpeggiate -> Just (M.arpeggiate [])
    _ -> Nothing

x_notations :: [N_Annotation] -> Maybe X.Content
x_notations xs =
  let n = mapMaybe x_notation xs
      o =
        catMaybes
          [ x_ornaments xs
          , x_technical xs
          , x_articulations xs
          {-,x_dynamics xs-}
          ]
  in case n ++ o of
      [] -> Nothing
      xs' -> Just (M.notations [] xs')

x_note_elem :: N_Annotation -> Maybe X.Content
x_note_elem x =
  case x of
    N_Pitch p -> Just (x_pitch p)
    N_Rest -> Just (M.rest [] [])
    N_Grace -> Just (M.grace [])
    N_Chord -> Just (M.chord [])
    _ -> Nothing

x_metronome :: T.Tempo_Marking -> X.Content
x_metronome (d, n) =
  let (T.Duration dv dt _) = d
      ty = T.whole_note_division_to_musicxml_type dv
      dt' = genericReplicate dt (M.dot [])
      n' = M.per_minute [] [M.cdata (show n)]
  in M.metronome [] ((M.beat_unit [] [M.cdata ty] : dt') ++ [n'])

x_pedal_type :: Pedal_Type -> X.Attr
x_pedal_type t =
  let n = case t of
        Pedal_Start -> "start"
        Pedal_Stop -> "stop"
        Pedal_Continue -> "continue"
        Pedal_Change -> "change"
  in M.type_A n

dynamic_mark_to_sound_value :: T.Dynamic_Mark -> Maybe Double
dynamic_mark_to_sound_value = fmap (* (10 / 9)) . T.dynamic_mark_midi

x_direction :: Direction_Type -> Maybe X.Content
x_direction x =
  case x of
    D_Dynamic_Mark mk ->
      let ty = M.direction_type [] [M.dynamics [] [x_dynamic_mark_t mk]]
          sn = fmap (x_sound . Sound_Dynamics) (dynamic_mark_to_sound_value mk)
          el = maybe [ty] ((ty :) . return) sn
      in Just (M.direction [x_placement_t Below] el)
    D_Hairpin T.Crescendo ->
      let ty = M.direction_type [] [M.wedge [M.type_A "crescendo"]]
      in Just (M.direction [x_placement_t Below] [ty])
    D_Hairpin T.Diminuendo ->
      let ty = M.direction_type [] [M.wedge [M.type_A "diminuendo"]]
      in Just (M.direction [x_placement_t Below] [ty])
    D_Hairpin T.End_Hairpin ->
      let ty = M.direction_type [] [M.wedge [M.type_A "stop"]]
      in Just (M.direction [x_placement_t Below] [ty])
    D_Rehearsal str ->
      let ty = M.direction_type [] [M.rehearsal [] [M.cdata str]]
      in Just (M.direction [] [ty])
    D_Words plc str ->
      let ty = M.direction_type [] [M.words' [] [M.cdata str]]
      in Just (M.direction [x_placement_t plc] [ty])
    D_Pedal pt _ _ ->
      let ty = M.direction_type [] [M.pedal [x_pedal_type pt]]
      in Just (M.direction [x_placement_t Below] [ty])
    D_Tempo_Marking m ->
      let ty = M.direction_type [] [x_metronome m]
      in Just (M.direction [x_placement_t Above] [ty])

x_n_direction :: N_Annotation -> Maybe X.Content
x_n_direction x =
  case x of
    N_Direction d -> x_direction d
    _ -> Nothing

x_m_direction :: M_Annotation -> Maybe X.Content
x_m_direction x =
  case x of
    M_Direction d -> x_direction d
    _ -> Nothing

x_sound :: Sound_Type -> X.Content
x_sound s =
  case s of
    Sound_Tempo n -> M.sound [M.tempo (show n)] []
    Sound_Dynamics n -> M.sound [M.dynamics' (show n)] []

{-
x_n_sound :: N_Annotation -> Maybe X.Content
x_n_sound x =
    case x of
      N_Sound s -> Just (x_sound s)
      _ -> Nothing
-}

x_accidental :: [N_Annotation] -> [X.Content]
x_accidental =
  let fn x = case x of
        N_Pitch p -> Just (x_pitch_accidental p)
        _ -> Nothing
  in mapMaybe fn

x_voice :: [N_Annotation] -> [X.Content]
x_voice =
  let fn x = case x of
        N_Voice i -> Just (M.voice [] [M.cdata (show i)])
        _ -> Nothing
  in mapMaybe fn

x_notehead_t :: Notehead_Type -> [X.Content]
x_notehead_t h =
  let s = case h of
        Notehead_Triangle -> "triangle"
        Notehead_Diamond -> "diamond"
        Notehead_Square -> "square"
        Notehead_Cross -> "cross"
  in [M.cdata s]

x_notehead :: [N_Annotation] -> [X.Content]
x_notehead =
  let fn x = case x of
        N_Notehead h -> Just (M.notehead [] (x_notehead_t h))
        _ -> Nothing
  in mapMaybe fn

x_staff :: [N_Annotation] -> [X.Content]
x_staff =
  let fn x = case x of
        N_Staff n -> Just (M.staff [] n)
        _ -> Nothing
  in mapMaybe fn

x_beam_t :: Beam_Type -> [X.Content]
x_beam_t b =
  let s = case b of
        Beam_Begin -> "begin"
        Beam_Continue -> "continue"
        Beam_End -> "end"
  in [M.cdata s]

x_beam :: [N_Annotation] -> [X.Content]
x_beam =
  let fn x = case x of
        N_Beam i j -> Just (M.beam [M.number (show i)] (x_beam_t j))
        _ -> Nothing
  in mapMaybe fn

x_note :: Note -> [X.Content]
x_note (Note d xs) =
  let (d', ty_dt, m) = x_duration d
      xs' = sort xs
      es = mapMaybe x_note_elem xs'
      nt = catMaybes [x_notations xs]
      dr = mapMaybe x_n_direction xs
      -- sn = mapMaybe x_n_sound xs
      ac = x_accidental xs
      vc = x_voice xs
      m' = maybe [] return m
      bm = x_beam xs
      nh = x_notehead xs
      st = x_staff xs
      n = M.note [] (concat [es, [d'], vc, ty_dt, ac, m', nh, st, bm, nt])
  in dr ++ {- sn ++ -} [n]

x_attribute :: M_Annotation -> Maybe X.Content
x_attribute x =
  case x of
    M_Division i -> Just (M.divisions [] [M.cdata (show i)])
    M_Clef c n -> Just (x_clef c n)
    M_Key n a m -> Just (x_key (n, a, m))
    M_Time_Signature i -> Just (x_time i)
    M_Staves n -> Just (M.staves [] n)
    M_Direction _ -> Nothing

x_attributes :: [M_Annotation] -> X.Content
x_attributes xs = M.attributes [] (mapMaybe x_attribute xs)

x_measure :: (Integer, Measure) -> X.Content
x_measure (i, Measure as ns) =
  let as' = sort as
      a = x_attributes as'
      dr = mapMaybe x_m_direction as'
      ns' = concatMap x_note ns
  in M.measure [M.number (show i)] (a : dr ++ ns')

x_part_name :: Name -> [X.Content]
x_part_name (nm, ab) =
  [ M.part_name [] [M.cdata nm]
  , M.part_abbreviation [] [M.cdata ab]
  ]

x_p_annotation :: P_Annotation -> [X.Content]
x_p_annotation x =
  case x of
    P_Name nm -> x_part_name nm

x_score_part :: Part -> X.Content
x_score_part p =
  case p of
    Part (Just i) as _ ->
      let as' = concatMap x_p_annotation as
          i' = 'P' : show i
      in M.score_part [M.id_A i'] as'
    _ -> error "x_score_part: no ID or GROUP"

x_group_name :: Name -> [X.Content]
x_group_name (nm, ab) =
  [ M.group_name [] [M.cdata nm]
  , M.group_abbreviation [] [M.cdata ab]
  ]

x_group_symbol_t :: Group_Symbol_Type -> X.Content
x_group_symbol_t x =
  let x' = map toLower (show x)
  in M.group_symbol [] [M.cdata x']

x_g_annotation :: G_Annotation -> [X.Content]
x_g_annotation x =
  case x of
    G_Name nm -> x_group_name nm
    G_Symbol sy -> [x_group_symbol_t sy]

x_part_group :: Part -> [X.Content]
x_part_group g =
  case g of
    Group (Just i) as ps ->
      let as' = concatMap x_g_annotation as
          i' = show i
          st = M.part_group [M.type_A "start", M.number i'] as'
          en = M.part_group [M.type_A "stop", M.number i'] []
      in [st] ++ map x_score_part ps ++ [en]
    _ -> error "x_part_group: no ID or PART"

x_part_list :: [Part] -> X.Content
x_part_list =
  let fn x = case x of
        Part {} -> [x_score_part x]
        Group {} -> x_part_group x
  in M.part_list [] . concatMap fn

x_part :: Part -> X.Content
x_part p =
  case p of
    Part (Just i) _ ms ->
      let i' = 'P' : show i
          ms' = P.m_set_divisions ms
      in M.part [M.id_A i'] (zipWith (curry x_measure) [1 ..] ms')
    _ -> error "x_part: no ID or GROUP"

x_score :: Score -> [X.Content]
x_score s =
  let (Score xs) = P.score_set_ids s
      pl = x_part_list xs
      f x = case x of
        Part {} -> [x]
        Group _ _ ps -> ps
      pt = map x_part (concatMap f xs)
  in pl : pt

{- | Make header from tuple of /title/, /number/, /dedication/ and
/composer/.
-}
x_header :: (String, String, String, String) -> [X.Content]
x_header (t, n, d, c) =
  let t' = M.work_title [] [M.cdata t]
      n' = M.work_number [] [M.cdata n]
      c' = M.creator [M.type_A "composer"] [M.cdata c]
      d' = M.credit_words [] [M.cdata d]
  in [ M.work [] [n', t']
     , M.identification [] [c']
     , M.credit [] [d']
     ]

x_score_partwise :: [X.Attr] -> [X.Content] -> X.Element
x_score_partwise z e = X.Element (X.unqual "score-partwise") z e Nothing

renderMusicXML :: [X.Content] -> String
renderMusicXML xs =
  unlines
    [ M.musicxml_xml
    , M.musicxml_partwise
    , X.ppElement (x_score_partwise [] xs)
    ]
