module Music.Typesetting.Output.MusicXml.Binding where

import qualified Text.XML.Light as X {- xml -}

type Elem = [X.Attr] -> [X.Content] -> X.Content
type Empty_Elem = [X.Attr] -> X.Content
type Pcdata_Elem = [X.Attr] -> String -> X.Content -- Parsed Character Data
type Numerical_Elem n = [X.Attr] -> n -> X.Content
type Attr = String -> X.Attr

-- | Ordinary character data, subject to escaping.
cdata :: String -> X.Content
cdata s = X.Text (X.CData X.CDataText s Nothing)

mk_elem :: String -> Elem
mk_elem nm z e = X.Elem (X.Element (X.unqual nm) z e Nothing)

mk_pcdata_elem :: String -> Pcdata_Elem
mk_pcdata_elem nm z s = mk_elem nm z [cdata s]

mk_numerical_elem :: Show n => String -> Numerical_Elem n
mk_numerical_elem nm z n = mk_elem nm z [cdata (show n)]

mk_empty_elem :: String -> Empty_Elem
mk_empty_elem nm z = X.Elem (X.Element (X.unqual nm) z [] Nothing)

mk_empty_elem_no_attr :: String -> X.Content
mk_empty_elem_no_attr nm = X.Elem (X.Element (X.unqual nm) [] [] Nothing)

mk_attr :: String -> Attr
mk_attr nm = X.Attr (X.unqual nm)

clef :: Elem
clef = mk_elem "clef"

type_A, id_A, number, spread, implicit, slash :: Attr
type_A = mk_attr "type"
id_A = mk_attr "id"
number = mk_attr "number"
spread = mk_attr "spead"
implicit = mk_attr "implicit"
slash = mk_attr "slash"

placement, bracket, show_number, show_type :: Attr
placement = mk_attr "placement"
bracket = mk_attr "bracket"
show_number = mk_attr "show-number"
show_type = mk_attr "show-type"

font_family, font_style, font_size, font_weight :: Attr
font_family = mk_attr "font-family"
font_style = mk_attr "font-style"
font_size = mk_attr "font-size"
font_weight = mk_attr "font-weight"

direction, direction_type, offset :: Elem
direction = mk_elem "direction"
direction_type = mk_elem "direction-type"
offset = mk_elem "offset"

wedge :: Empty_Elem
wedge = mk_empty_elem "wedge"

rehearsal :: Elem
rehearsal = mk_elem "rehearsal"

words' :: Elem
words' = mk_elem "words"

pedal :: Empty_Elem
pedal = mk_empty_elem "pedal"

sign :: Elem
sign = mk_elem "sign"

line :: Elem
line = mk_elem "line"

clef_octave_change :: Elem
clef_octave_change = mk_elem "clef-octave-change"

score_partwise :: Elem
score_partwise = mk_elem "score-partwise"

work, work_number, work_title :: Elem
work = mk_elem "work"
work_title = mk_elem "work-title"
work_number = mk_elem "work-number"

identification, creator, rights :: Elem
identification = mk_elem "identification"
creator = mk_elem "creator"
rights = mk_elem "rights"

credit, credit_words :: Elem
credit = mk_elem "credit"
credit_words = mk_elem "credit-words"

part_list, part_group, score_part :: Elem
part_list = mk_elem "part-list"
part_group = mk_elem "part-group"
score_part = mk_elem "score-part"

part_name, part_abbreviation, score_instrument :: Elem
part_name = mk_elem "part-name"
part_abbreviation = mk_elem "part-abbreviation"
score_instrument = mk_elem "score-instrument"

group_name, group_abbreviation, group_symbol, group_barline :: Elem
group_name = mk_elem "group-name"
group_abbreviation = mk_elem "group-abbreviation"
group_symbol = mk_elem "group-symbol"
group_barline = mk_elem "group-barline"

instrument_name :: Elem
instrument_name = mk_elem "instrument-name"

part, measure, attributes, divisions :: Elem
part = mk_elem "part"
measure = mk_elem "measure"
attributes = mk_elem "attributes"
divisions = mk_elem "divisions"

key, fifths, mode :: Elem
key = mk_elem "key"
fifths = mk_elem "fifths"
mode = mk_elem "mode"

time, beats, beat_type :: Elem
time = mk_elem "time"
beats = mk_elem "beats"
beat_type = mk_elem "beat-type"

note, pitch, step, alter, octave, rest, duration, voice, type_E, stem, beam, notehead, accidental :: Elem
note = mk_elem "note"
pitch = mk_elem "pitch"
step = mk_elem "step"
alter = mk_elem "alter"
octave = mk_elem "octave"
rest = mk_elem "rest"
duration = mk_elem "duration"
voice = mk_elem "voice"
type_E = mk_elem "type"
stem = mk_elem "stem"
beam = mk_elem "beam"
notehead = mk_elem "notehead"
accidental = mk_elem "accidental"

staves, staff :: Show n => Numerical_Elem n
staves = mk_numerical_elem "staves"
staff = mk_numerical_elem "staff"

notations, ornaments, technical, dynamics, tremolo :: Elem
notations = mk_elem "notations"
ornaments = mk_elem "ornaments"
technical = mk_elem "technical"
dynamics = mk_elem "dynamics"
tremolo = mk_elem "tremolo"

sound :: Elem
sound = mk_elem "sound"

tempo, dynamics' :: Attr
tempo = mk_attr "tempo"
dynamics' = mk_attr "dynamics"

arpeggiate, tie, tied, slur :: Empty_Elem
arpeggiate = mk_empty_elem "arpeggiate" -- up/down
tie = mk_empty_elem "tie"
tied = mk_empty_elem "tied"
slur = mk_empty_elem "slur"

slide, glissando :: Elem
slide = mk_elem "slide"
glissando = mk_elem "glissando"

fermata :: Elem
fermata = mk_elem "fermata"

harmonic :: Elem
harmonic = mk_elem "harmonic"

natural, artificial :: Empty_Elem
natural = mk_empty_elem "natural"
artificial = mk_empty_elem "artificial"

up_bow, down_bow :: Empty_Elem
up_bow = mk_empty_elem "up-bow"
down_bow = mk_empty_elem "down-bow"

open_string, stopped, snap_pizzicato :: Empty_Elem
open_string = mk_empty_elem "open-string"
stopped = mk_empty_elem "stopped"
snap_pizzicato = mk_empty_elem "snap-pizzicato"

other_technical :: Pcdata_Elem
other_technical = mk_pcdata_elem "other-technical"

forward, backup :: Elem
forward = mk_elem "forward"
backup = mk_elem "backup"

chord, cue, grace, dot :: Empty_Elem
chord = mk_empty_elem "chord"
cue = mk_empty_elem "cue"
grace = mk_empty_elem "grace"
dot = mk_empty_elem "dot"

time_modification, actual_notes, normal_notes, normal_type, normal_dot :: Elem
time_modification = mk_elem "time-modification"
actual_notes = mk_elem "actual-notes"
normal_notes = mk_elem "normal-notes"
normal_type = mk_elem "normal-type"
normal_dot = mk_elem "normal-dot"

tuplet, tuplet_actual, tuplet_normal, tuplet_number, tuplet_type :: Elem
tuplet = mk_elem "tuplet"
tuplet_actual = mk_elem "tuplet-actual"
tuplet_normal = mk_elem "tuplet-normal"
tuplet_number = mk_elem "tuplet-number"
tuplet_type = mk_elem "tuplet-type"

tuplet_dot :: Empty_Elem
tuplet_dot = mk_empty_elem "tuplet-dot"

articulations :: Elem
articulations = mk_elem "articulations"

accent :: Empty_Elem
accent = mk_empty_elem "accent"

metronome, beat_unit, per_minute :: Elem
metronome = mk_elem "metronome"
beat_unit = mk_elem "beat-unit"
per_minute = mk_elem "per-minute"

beat_unit_dot :: Empty_Elem
beat_unit_dot = mk_empty_elem "beat-unit-dot"

-- * String constants

musicxml_xml :: String
musicxml_xml = "<?xml version=\"1.0\" standalone=\"no\"?>"

type DocType = String

musicxml_partwise :: DocType
musicxml_partwise = "<!DOCTYPE score-partwise PUBLIC \"-//Recordare//DTD MusicXML 1.0 Partwise//EN\" \"http://www.musicxml.org/dtds/partwise.dtd\">"
