module Music.Typesetting.Process where

import Data.List {- base -}
import Data.Ratio {- base -}

import qualified Music.Typesetting.Literal.Dynamic as D {- hts -}
import Music.Typesetting.Model {- hts -}
import qualified Music.Typesetting.Query as Q {- hts -}

-- * Clef

-- | If 'M_Annotation' is a 'M_Clef' set the staff number.
m_clef_set_staff :: Integer -> M_Annotation -> Maybe M_Annotation
m_clef_set_staff n a =
  case a of
    M_Clef c _ -> Just (M_Clef c n)
    _ -> Nothing

-- * Note

-- | Remove any tie annotations at note
n_remove_ties :: Note -> Note
n_remove_ties (Note d a) =
  let f e = e == N_End_Tied || e == N_Begin_Tied
      a' = filter (not . f) a
  in Note d a'

{- | Process a 'Note' sequence adding 'D_End_Hairpin' annotations as
required, ie. where there is an open hairpin annotation and a
'Note' has a dyamic annotation without a close hairpin annotation.
-}
n_add_end_hairpins :: [Note] -> [Note]
n_add_end_hairpins =
  let f st n =
        case n of
          [] -> []
          [Note d a] -> [Note d (if st then D.end_hairpin : a else a)]
          Note d a : n' ->
            let (a', st') =
                  if st
                    && any Q.na_is_begin_dynamic a
                    && D.end_hairpin `notElem` a
                    then
                      ( D.end_hairpin : a
                      , any Q.na_is_begin_hairpin a
                      )
                    else
                      ( a
                      , st || any Q.na_is_begin_hairpin a
                      )
            in Note d a' : f st' n'
  in f False

-- * Duration / Division

x_divisions :: Integer
x_divisions = 5 * 7 * 8 * 9 * 11 * 12

m_set_divisions :: [Measure] -> [Measure]
m_set_divisions xs =
  case xs of
    [] -> error "m_set_divisions: no measures?"
    Measure as ms : xs' -> Measure (M_Division x_divisions : as) ms : xs'

duration_rq_to_dv :: Rational -> Integer
duration_rq_to_dv x =
  let n = x * toRational x_divisions
  in if denominator n == 1
      then numerator n
      else error ("duration_rq_to_dv: non integer duration" ++ show n)

-- * Measure

-- | Delete persistent annotations or like.
prune :: (a -> a -> Bool) -> (b -> Maybe a) -> (a -> b -> b) -> [b] -> [b]
prune cmp get del =
  let go _ [] = []
      go Nothing (x : xs) = x : go (get x) xs
      go (Just st) (x : xs) =
        case get x of
          Nothing -> x : go (Just st) xs
          Just y ->
            if cmp st y
              then del y x : go (Just st) xs
              else x : go (Just y) xs
  in go Nothing

m_delete_annotation :: M_Annotation -> Measure -> Measure
m_delete_annotation i (Measure a n) = Measure (delete i a) n

m_remove_duplicate_ts :: [Measure] -> [Measure]
m_remove_duplicate_ts =
  let get = Q.m_time_signature'
      del = m_delete_annotation
  in prune (==) get del

m_remove_duplicate_tempo_marking :: [Measure] -> [Measure]
m_remove_duplicate_tempo_marking =
  let get = Q.m_tempo_marking'
      del = m_delete_annotation
  in prune (==) get del

-- * ID

part_set_id :: (ID, Part) -> (ID, Part)
part_set_id (i, x) =
  case x of
    (Part Nothing pa vs) -> (i + 1, Part (Just i) pa vs)
    (Group Nothing ga ps) ->
      let (i', ps') = mapAccumL (curry part_set_id) (i + 1) ps
      in (i', Group (Just i) ga ps')
    _ -> error "part_set_id: has ID"

score_set_ids :: Score -> Score
score_set_ids (Score xs) =
  let (_, xs') = mapAccumL (curry part_set_id) 0 xs
  in Score xs'
