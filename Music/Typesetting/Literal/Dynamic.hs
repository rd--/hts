-- | Dynamic mark literals.
module Music.Typesetting.Literal.Dynamic where

import Music.Theory.Dynamic_Mark {- hmt -}

import Music.Typesetting.Literal
import Music.Typesetting.Model

niente, pppp, ppp, pp, p, mp, mf, f, ff, fff, ffff, fp, sfz :: N_Annotation
niente = dynamic_mark Niente
pppp = dynamic_mark Pppp
ppp = dynamic_mark Ppp
pp = dynamic_mark Pp
p = dynamic_mark P
mp = dynamic_mark Mp
mf = dynamic_mark Mf
f = dynamic_mark F
ff = dynamic_mark Ff
fff = dynamic_mark Fff
ffff = dynamic_mark Ffff
fp = dynamic_mark Fp
sfz = dynamic_mark Sfz

cresc, dim, end_hairpin :: N_Annotation
cresc = N_Direction (D_Hairpin Crescendo)
dim = N_Direction (D_Hairpin Diminuendo)
end_hairpin = N_Direction (D_Hairpin End_Hairpin)
