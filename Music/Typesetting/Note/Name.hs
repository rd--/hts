-- | 'Pitch' names lifted to 'Note' values.
module Music.Typesetting.Note.Name where

import qualified Music.Theory.Duration.Name as T {- hmt -}
import qualified Music.Theory.Pitch as T
import qualified Music.Theory.Pitch.Name as T

import Music.Typesetting.Model

-- * Notes

pitch_to_note :: T.Pitch -> Note
pitch_to_note p = Note T.quarter_note [N_Pitch p]

a0, b0 :: Note
a0 = pitch_to_note T.a0
b0 = pitch_to_note T.b0

bes0 :: Note
bes0 = pitch_to_note T.bes0

ais0, bis0 :: Note
ais0 = pitch_to_note T.ais0
bis0 = pitch_to_note T.bis0

c1, d1, e1, f1, g1, a1, b1 :: Note
c1 = pitch_to_note T.c1
d1 = pitch_to_note T.d1
e1 = pitch_to_note T.e1
f1 = pitch_to_note T.f1
g1 = pitch_to_note T.g1
a1 = pitch_to_note T.a1
b1 = pitch_to_note T.b1

ces1, des1, ees1, fes1, ges1, aes1, bes1 :: Note
ces1 = pitch_to_note T.ces1
des1 = pitch_to_note T.des1
ees1 = pitch_to_note T.ees1
fes1 = pitch_to_note T.fes1
ges1 = pitch_to_note T.ges1
aes1 = pitch_to_note T.aes1
bes1 = pitch_to_note T.bes1

cis1, dis1, eis1, fis1, gis1, ais1, bis1 :: Note
cis1 = pitch_to_note T.cis1
dis1 = pitch_to_note T.dis1
eis1 = pitch_to_note T.eis1
fis1 = pitch_to_note T.fis1
gis1 = pitch_to_note T.gis1
ais1 = pitch_to_note T.ais1
bis1 = pitch_to_note T.bis1

c2, d2, e2, f2, g2, a2, b2 :: Note
c2 = pitch_to_note T.c2
d2 = pitch_to_note T.d2
e2 = pitch_to_note T.e2
f2 = pitch_to_note T.f2
g2 = pitch_to_note T.g2
a2 = pitch_to_note T.a2
b2 = pitch_to_note T.b2

ces2, des2, ees2, fes2, ges2, aes2, bes2 :: Note
ces2 = pitch_to_note T.ces2
des2 = pitch_to_note T.des2
ees2 = pitch_to_note T.ees2
fes2 = pitch_to_note T.fes2
ges2 = pitch_to_note T.ges2
aes2 = pitch_to_note T.aes2
bes2 = pitch_to_note T.bes2

cis2, dis2, eis2, fis2, gis2, ais2, bis2 :: Note
cis2 = pitch_to_note T.cis2
dis2 = pitch_to_note T.dis2
eis2 = pitch_to_note T.eis2
fis2 = pitch_to_note T.fis2
gis2 = pitch_to_note T.gis2
ais2 = pitch_to_note T.ais2
bis2 = pitch_to_note T.bis2

cisis2, disis2, eisis2, fisis2, gisis2, aisis2, bisis2 :: Note
cisis2 = pitch_to_note T.cisis2
disis2 = pitch_to_note T.disis2
eisis2 = pitch_to_note T.eisis2
fisis2 = pitch_to_note T.fisis2
gisis2 = pitch_to_note T.gisis2
aisis2 = pitch_to_note T.aisis2
bisis2 = pitch_to_note T.bisis2

c3, d3, e3, f3, g3, a3, b3 :: Note
c3 = pitch_to_note T.c3
d3 = pitch_to_note T.d3
e3 = pitch_to_note T.e3
f3 = pitch_to_note T.f3
g3 = pitch_to_note T.g3
a3 = pitch_to_note T.a3
b3 = pitch_to_note T.b3

ces3, des3, ees3, fes3, ges3, aes3, bes3 :: Note
ces3 = pitch_to_note T.ces3
des3 = pitch_to_note T.des3
ees3 = pitch_to_note T.ees3
fes3 = pitch_to_note T.fes3
ges3 = pitch_to_note T.ges3
aes3 = pitch_to_note T.aes3
bes3 = pitch_to_note T.bes3

cis3, dis3, eis3, fis3, gis3, ais3, bis3 :: Note
cis3 = pitch_to_note T.cis3
dis3 = pitch_to_note T.dis3
eis3 = pitch_to_note T.eis3
fis3 = pitch_to_note T.fis3
gis3 = pitch_to_note T.gis3
ais3 = pitch_to_note T.ais3
bis3 = pitch_to_note T.bis3

cisis3, disis3, eisis3, fisis3, gisis3, aisis3, bisis3 :: Note
cisis3 = pitch_to_note T.cisis3
disis3 = pitch_to_note T.disis3
eisis3 = pitch_to_note T.eisis3
fisis3 = pitch_to_note T.fisis3
gisis3 = pitch_to_note T.gisis3
aisis3 = pitch_to_note T.aisis3
bisis3 = pitch_to_note T.bisis3

ceseh3, deseh3, eeseh3, feseh3, geseh3, aeseh3, beseh3 :: Note
ceseh3 = pitch_to_note T.ceseh3
deseh3 = pitch_to_note T.deseh3
eeseh3 = pitch_to_note T.eeseh3
feseh3 = pitch_to_note T.feseh3
geseh3 = pitch_to_note T.geseh3
aeseh3 = pitch_to_note T.aeseh3
beseh3 = pitch_to_note T.beseh3

ceh3, deh3, eeh3, feh3, geh3, aeh3, beh3 :: Note
ceh3 = pitch_to_note T.ceh3
deh3 = pitch_to_note T.deh3
eeh3 = pitch_to_note T.eeh3
feh3 = pitch_to_note T.feh3
geh3 = pitch_to_note T.geh3
aeh3 = pitch_to_note T.aeh3
beh3 = pitch_to_note T.beh3

cih3, dih3, eih3, fih3, gih3, aih3, bih3 :: Note
cih3 = pitch_to_note T.cih3
dih3 = pitch_to_note T.dih3
eih3 = pitch_to_note T.eih3
fih3 = pitch_to_note T.fih3
gih3 = pitch_to_note T.gih3
aih3 = pitch_to_note T.aih3
bih3 = pitch_to_note T.bih3

cisih3, disih3, eisih3, fisih3, gisih3, aisih3, bisih3 :: Note
cisih3 = pitch_to_note T.cisih3
disih3 = pitch_to_note T.disih3
eisih3 = pitch_to_note T.eisih3
fisih3 = pitch_to_note T.fisih3
gisih3 = pitch_to_note T.gisih3
aisih3 = pitch_to_note T.aisih3
bisih3 = pitch_to_note T.bisih3

c4, d4, e4, f4, g4, a4, b4 :: Note
c4 = pitch_to_note T.c4
d4 = pitch_to_note T.d4
e4 = pitch_to_note T.e4
f4 = pitch_to_note T.f4
g4 = pitch_to_note T.g4
a4 = pitch_to_note T.a4
b4 = pitch_to_note T.b4

ces4, des4, ees4, fes4, ges4, aes4, bes4 :: Note
ces4 = pitch_to_note T.ces4
des4 = pitch_to_note T.des4
ees4 = pitch_to_note T.ees4
fes4 = pitch_to_note T.fes4
ges4 = pitch_to_note T.ges4
aes4 = pitch_to_note T.aes4
bes4 = pitch_to_note T.bes4

cis4, dis4, eis4, fis4, gis4, ais4, bis4 :: Note
cis4 = pitch_to_note T.cis4
dis4 = pitch_to_note T.dis4
eis4 = pitch_to_note T.eis4
fis4 = pitch_to_note T.fis4
gis4 = pitch_to_note T.gis4
ais4 = pitch_to_note T.ais4
bis4 = pitch_to_note T.bis4

ceses4, deses4, eeses4, feses4, geses4, aeses4, beses4 :: Note
ceses4 = pitch_to_note T.ceses4
deses4 = pitch_to_note T.deses4
eeses4 = pitch_to_note T.eeses4
feses4 = pitch_to_note T.feses4
geses4 = pitch_to_note T.geses4
aeses4 = pitch_to_note T.aeses4
beses4 = pitch_to_note T.beses4

cisis4, disis4, eisis4, fisis4, gisis4, aisis4, bisis4 :: Note
cisis4 = pitch_to_note T.cisis4
disis4 = pitch_to_note T.disis4
eisis4 = pitch_to_note T.eisis4
fisis4 = pitch_to_note T.fisis4
gisis4 = pitch_to_note T.gisis4
aisis4 = pitch_to_note T.aisis4
bisis4 = pitch_to_note T.bisis4

ceseh4, deseh4, eeseh4, feseh4, geseh4, aeseh4, beseh4 :: Note
ceseh4 = pitch_to_note T.ceseh4
deseh4 = pitch_to_note T.deseh4
eeseh4 = pitch_to_note T.eeseh4
feseh4 = pitch_to_note T.feseh4
geseh4 = pitch_to_note T.geseh4
aeseh4 = pitch_to_note T.aeseh4
beseh4 = pitch_to_note T.beseh4

ceh4, deh4, eeh4, feh4, geh4, aeh4, beh4 :: Note
ceh4 = pitch_to_note T.ceh4
deh4 = pitch_to_note T.deh4
eeh4 = pitch_to_note T.eeh4
feh4 = pitch_to_note T.feh4
geh4 = pitch_to_note T.geh4
aeh4 = pitch_to_note T.aeh4
beh4 = pitch_to_note T.beh4

cih4, dih4, eih4, fih4, gih4, aih4, bih4 :: Note
cih4 = pitch_to_note T.cih4
dih4 = pitch_to_note T.dih4
eih4 = pitch_to_note T.eih4
fih4 = pitch_to_note T.fih4
gih4 = pitch_to_note T.gih4
aih4 = pitch_to_note T.aih4
bih4 = pitch_to_note T.bih4

cisih4, disih4, eisih4, fisih4, gisih4, aisih4, bisih4 :: Note
cisih4 = pitch_to_note T.cisih4
disih4 = pitch_to_note T.disih4
eisih4 = pitch_to_note T.eisih4
fisih4 = pitch_to_note T.fisih4
gisih4 = pitch_to_note T.gisih4
aisih4 = pitch_to_note T.aisih4
bisih4 = pitch_to_note T.bisih4

c5, d5, e5, f5, g5, a5, b5 :: Note
c5 = pitch_to_note T.c5
d5 = pitch_to_note T.d5
e5 = pitch_to_note T.e5
f5 = pitch_to_note T.f5
g5 = pitch_to_note T.g5
a5 = pitch_to_note T.a5
b5 = pitch_to_note T.b5

ces5, des5, ees5, fes5, ges5, aes5, bes5 :: Note
ces5 = pitch_to_note T.ces5
des5 = pitch_to_note T.des5
ees5 = pitch_to_note T.ees5
fes5 = pitch_to_note T.fes5
ges5 = pitch_to_note T.ges5
aes5 = pitch_to_note T.aes5
bes5 = pitch_to_note T.bes5

cis5, dis5, eis5, fis5, gis5, ais5, bis5 :: Note
cis5 = pitch_to_note T.cis5
dis5 = pitch_to_note T.dis5
eis5 = pitch_to_note T.eis5
fis5 = pitch_to_note T.fis5
gis5 = pitch_to_note T.gis5
ais5 = pitch_to_note T.ais5
bis5 = pitch_to_note T.bis5

ceses5, deses5, eeses5, feses5, geses5, aeses5, beses5 :: Note
ceses5 = pitch_to_note T.ceses5
deses5 = pitch_to_note T.deses5
eeses5 = pitch_to_note T.eeses5
feses5 = pitch_to_note T.feses5
geses5 = pitch_to_note T.geses5
aeses5 = pitch_to_note T.aeses5
beses5 = pitch_to_note T.beses5

cisis5, disis5, eisis5, fisis5, gisis5, aisis5, bisis5 :: Note
cisis5 = pitch_to_note T.cisis5
disis5 = pitch_to_note T.disis5
eisis5 = pitch_to_note T.eisis5
fisis5 = pitch_to_note T.fisis5
gisis5 = pitch_to_note T.gisis5
aisis5 = pitch_to_note T.aisis5
bisis5 = pitch_to_note T.bisis5

ceseh5, deseh5, eeseh5, feseh5, geseh5, aeseh5, beseh5 :: Note
ceseh5 = pitch_to_note T.ceseh5
deseh5 = pitch_to_note T.deseh5
eeseh5 = pitch_to_note T.eeseh5
feseh5 = pitch_to_note T.feseh5
geseh5 = pitch_to_note T.geseh5
aeseh5 = pitch_to_note T.aeseh5
beseh5 = pitch_to_note T.beseh5

ceh5, deh5, eeh5, feh5, geh5, aeh5, beh5 :: Note
ceh5 = pitch_to_note T.ceh5
deh5 = pitch_to_note T.deh5
eeh5 = pitch_to_note T.eeh5
feh5 = pitch_to_note T.feh5
geh5 = pitch_to_note T.geh5
aeh5 = pitch_to_note T.aeh5
beh5 = pitch_to_note T.beh5

cih5, dih5, eih5, fih5, gih5, aih5, bih5 :: Note
cih5 = pitch_to_note T.cih5
dih5 = pitch_to_note T.dih5
eih5 = pitch_to_note T.eih5
fih5 = pitch_to_note T.fih5
gih5 = pitch_to_note T.gih5
aih5 = pitch_to_note T.aih5
bih5 = pitch_to_note T.bih5

cisih5, disih5, eisih5, fisih5, gisih5, aisih5, bisih5 :: Note
cisih5 = pitch_to_note T.cisih5
disih5 = pitch_to_note T.disih5
eisih5 = pitch_to_note T.eisih5
fisih5 = pitch_to_note T.fisih5
gisih5 = pitch_to_note T.gisih5
aisih5 = pitch_to_note T.aisih5
bisih5 = pitch_to_note T.bisih5

c6, d6, e6, f6, g6, a6, b6 :: Note
c6 = pitch_to_note T.c6
d6 = pitch_to_note T.d6
e6 = pitch_to_note T.e6
f6 = pitch_to_note T.f6
g6 = pitch_to_note T.g6
a6 = pitch_to_note T.a6
b6 = pitch_to_note T.b6

ces6, des6, ees6, fes6, ges6, aes6, bes6 :: Note
ces6 = pitch_to_note T.ces6
des6 = pitch_to_note T.des6
ees6 = pitch_to_note T.ees6
fes6 = pitch_to_note T.fes6
ges6 = pitch_to_note T.ges6
aes6 = pitch_to_note T.aes6
bes6 = pitch_to_note T.bes6

cis6, dis6, eis6, fis6, gis6, ais6, bis6 :: Note
cis6 = pitch_to_note T.cis6
dis6 = pitch_to_note T.dis6
eis6 = pitch_to_note T.eis6
fis6 = pitch_to_note T.fis6
gis6 = pitch_to_note T.gis6
ais6 = pitch_to_note T.ais6
bis6 = pitch_to_note T.bis6

ceseh6, deseh6, eeseh6, feseh6, geseh6, aeseh6, beseh6 :: Note
ceseh6 = pitch_to_note T.ceseh6
deseh6 = pitch_to_note T.deseh6
eeseh6 = pitch_to_note T.eeseh6
feseh6 = pitch_to_note T.feseh6
geseh6 = pitch_to_note T.geseh6
aeseh6 = pitch_to_note T.aeseh6
beseh6 = pitch_to_note T.beseh6

ceh6, deh6, eeh6, feh6, geh6, aeh6, beh6 :: Note
ceh6 = pitch_to_note T.ceh6
deh6 = pitch_to_note T.deh6
eeh6 = pitch_to_note T.eeh6
feh6 = pitch_to_note T.feh6
geh6 = pitch_to_note T.geh6
aeh6 = pitch_to_note T.aeh6
beh6 = pitch_to_note T.beh6

cih6, dih6, eih6, fih6, gih6, aih6, bih6 :: Note
cih6 = pitch_to_note T.cih6
dih6 = pitch_to_note T.dih6
eih6 = pitch_to_note T.eih6
fih6 = pitch_to_note T.fih6
gih6 = pitch_to_note T.gih6
aih6 = pitch_to_note T.aih6
bih6 = pitch_to_note T.bih6

cisih6, disih6, eisih6, fisih6, gisih6, aisih6, bisih6 :: Note
cisih6 = pitch_to_note T.cisih6
disih6 = pitch_to_note T.disih6
eisih6 = pitch_to_note T.eisih6
fisih6 = pitch_to_note T.fisih6
gisih6 = pitch_to_note T.gisih6
aisih6 = pitch_to_note T.aisih6
bisih6 = pitch_to_note T.bisih6

c7, d7, e7, f7, g7, a7, b7 :: Note
c7 = pitch_to_note T.c7
d7 = pitch_to_note T.d7
e7 = pitch_to_note T.e7
f7 = pitch_to_note T.f7
g7 = pitch_to_note T.g7
a7 = pitch_to_note T.a7
b7 = pitch_to_note T.b7

ces7, des7, ees7, fes7, ges7, aes7, bes7 :: Note
ces7 = pitch_to_note T.ces7
des7 = pitch_to_note T.des7
ees7 = pitch_to_note T.ees7
fes7 = pitch_to_note T.fes7
ges7 = pitch_to_note T.ges7
aes7 = pitch_to_note T.aes7
bes7 = pitch_to_note T.bes7

cis7, dis7, eis7, fis7, gis7, ais7, bis7 :: Note
cis7 = pitch_to_note T.cis7
dis7 = pitch_to_note T.dis7
eis7 = pitch_to_note T.eis7
fis7 = pitch_to_note T.fis7
gis7 = pitch_to_note T.gis7
ais7 = pitch_to_note T.ais7
bis7 = pitch_to_note T.bis7

c8, cis8, d8 :: Note
c8 = pitch_to_note T.c8
cis8 = pitch_to_note T.cis8
d8 = pitch_to_note T.d8
