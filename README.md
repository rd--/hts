hts - haskell music typesetting
-------------------------------

A [haskell](http://haskell.org/) music notation model
with output to [MusicXML](http://www.makemusic.com/musicxml/).

See [hly](?t=hly) for an alternative
writing to [lilypond](http://lilypond.org/)

© [rohan drape](http://rohandrape.net/), 2010-2024, [gpl](http://gnu.org/copyleft/)
